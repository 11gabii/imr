using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class check_colider : MonoBehaviour
{
    public GameObject ball;
    public GameObject parent;
    public GameObject collisionValidator;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.G))
        {
            ball.GetComponent<Rigidbody>().AddForce(parent.transform.forward * 1000);
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.name == collisionValidator.gameObject.name)
        {
            collisionValidator.GetComponent<Renderer>().material.SetColor("_Color", Color.cyan);
            Debug.Log("You scored! Congrats!");
        }
    }
}
