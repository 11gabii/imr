using UnityEngine;

public class ManageAnimations : MonoBehaviour
{
    public GameObject cactus1;
    public GameObject cactus2;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance (cactus1.transform.position, cactus2.transform.position) > 0.1) {
            cactus1.GetComponent<Animator>().SetBool("isDead",false);
            cactus2.GetComponent<Animator>().SetBool("isDead",false);
            Debug.Log(Vector3.Distance (cactus1.transform.position, cactus2.transform.position));
        }
        else
        {
            cactus1.GetComponent<Animator>().SetBool("isDead",true);
            cactus2.GetComponent<Animator>().SetBool("isDead",true); 
        }
    }
}
